import numpy as np
import cv2

img1 = cv2.imread(fr"G:\Parth`s Space\Internship\data\color spaces\fireman1.jpg")
img2 = cv2.imread(fr"G:\Parth`s Space\Internship\data\color spaces\fireman2.jpg")

resized_img1 =cv2.resize(img1 ,(400 , 400) , interpolation=cv2.INTER_AREA )
resized_img2 =cv2.resize(img2,(400 , 400) , interpolation=cv2.INTER_AREA )

resized_img1 = cv2.putText(resized_img1,"image1", (10 , 25), cv2.FONT_HERSHEY_SIMPLEX, 1, 0)
resized_img2 = cv2.putText(resized_img2,"image2", (10 , 25), cv2.FONT_HERSHEY_SIMPLEX, 1, 0)

stack = np.hstack((resized_img1,resized_img2))

cv2.imshow("stack_img" , stack)
cv2.waitKey(0)
cv2.destroyAllWindows()

"""
YCrCb Color-Space  - mainly used in TV broadcasting
The YCrCb color space is derived from the RGB color space and has the following three compoenents.

Y – Luminance or Luma component obtained from RGB after gamma correction.(gray scale) (0 - 255)
Cr = R – Y(how far is the red component from Luma ).(redness vale)(less red in image more green Cr will be)(-127,+128)
Cb = B – Y(how far is the blue component from Luma ).(blueness value)(less blue in image more yellow Cr will be)(-127,+128)

"""

img1_ycrcb = cv2.cvtColor(resized_img1, cv2.COLOR_RGB2YCrCb)
img2_ycrcb = cv2.cvtColor(resized_img2, cv2.COLOR_RGB2YCrCb)

stack_ycrcb = np.hstack((img1_ycrcb , img2_ycrcb))

cv2.imshow("stack_ycrcb" ,stack_ycrcb)
cv2.waitKey(0)
cv2.destroyAllWindows()


y  = img1_ycrcb[: , : , 0]
Cr = img1_ycrcb[: , : , 1]
Cb = img1_ycrcb[: , : , 2]

img1_y_cr_cb = np.hstack((y , Cr , Cb))


cv2.imshow("img1_y_cr_cb" ,img1_y_cr_cb)
cv2.imshow("img1" ,resized_img1 )
cv2.waitKey(0)
cv2.destroyAllWindows()



""""
LAB color space
The Lab color space has three components.

L – Lightness ( Intensity ).
a – color component ranging from Green to Magenta.
b – color component ranging from Blue to Yellow.
"""
img1_hsv = cv2.cvtColor(resized_img1, cv2.COLOR_RGB2LAB)
img2_hsv  = cv2.cvtColor(resized_img2, cv2.COLOR_RGB2LAB)

stack_lab  = np.hstack((img1_hsv  , img2_hsv))

cv2.imshow("stack_ycrcb" ,stack_lab )
cv2.waitKey(0)
cv2.destroyAllWindows()


l = img1_hsv[: , : , 0]
a = img1_hsv[: , : , 1]
b = img1_hsv[: , : , 2]

indoor_Stack= np.hstack((l , a , b))


cv2.imshow("indoor_lab" ,indoor_Stack)
cv2.imshow("indoor" ,resized_img1 )
cv2.waitKey(0)
cv2.destroyAllWindows()






"""
HSV Color Space
The HSV color space has the following three components

H – Hue ( Dominant Wavelength ).
S – Saturation ( Purity / shades of the color ).
V – Value ( Intensity ).

HSV color space: It stores color information in a cylindrical representation of RGB color points.
It attempts to depict the colors as perceived by the human eye. Hue value varies from 0-179,
Saturation value varies from 0-255 and Value value varies from 0-255. It is mostly used for color segmentation purpose.
"""
img1_hsv = cv2.cvtColor(resized_img1, cv2.COLOR_RGB2HSV)
img2_hsv  = cv2.cvtColor(resized_img2, cv2.COLOR_RGB2HSV)

stack_hsv = np.hstack((img1_hsv  , img2_hsv))

cv2.imshow("stack_hsv" ,stack_hsv )
cv2.waitKey(0)
cv2.destroyAllWindows()


h = img1_hsv[: , : , 0]
s= img1_hsv[: , : , 1]
v = img1_hsv[: , : , 2]

Stack= np.hstack((h , s , v))

cv2.imshow("stack_hsv" ,Stack )
cv2.waitKey(0)
cv2.destroyAllWindows()




