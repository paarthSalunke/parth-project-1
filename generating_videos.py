import cv2
import numpy as np
import os


print(os.listdir(fr"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts"))

files = os.listdir(fr"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts")

#extracting image path for each image form dir
images = []
text_files = []
for i in files:
    if i[-3:] == "jpg":
        path_im = fr"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts\{i}"
        images.append(path_im)
    else:text_files.append(i)

#another way to extract images files from dir
files_images = [img for img in files if img.endswith("jpg") ]
print(files_images)


image1 = cv2.imread(images[0])
print(image1.shape)

#height and width
height =image1.shape[0]
width =image1.shape[1]
print(width,height)

#video write
video_name = 'video.avi'
video = cv2.VideoWriter(video_name, 0, 1, (width,height))

#writing each images
for img in images:
    video.write(cv2.imread(img))

cv2.destroyAllWindows()
video.release()


#read output video

vid_capture = cv2.VideoCapture(fr'G:\Parth`s Space\Internship\video.avi')

ret, frame = vid_capture.read()
print(ret)
print(np.shape(frame))

while(vid_capture.isOpened()):
    #reading each frame
    ret, frame = vid_capture.read()

    if ret == True:
        #viz each frame in seq
        cv2.imshow('Frame', frame)

        # waitKey() function to pause for 20ms between video frames.
        # Calling the waitKey() function lets you monitor the keyboard for user input.
        # In this case, for example, if the user presses the ‘q’ key, you exit the loop.
        key = cv2.waitKey(20)

        if key == ord('q'):
            break
    else: break

# Release the video capture object
vid_capture.release()
cv2.destroyAllWindows()








