"""3-5. Changing Guest List: You just heard that one of your guests can’t make the
dinner, so you need to send out a new set of invitations. You’ll have to think of
someone else to invite.
•	 Start with your program from Exercise 3-4. Add a print() call at the end
of your program stating the name of the guest who can’t make it.
•	 Modify your list, replacing the name of the guest who can’t make it with
the name of the new person you are inviting.
•	 Print a second set of invitation messages, one for each person who is still
in your list"""

guest = ["parth" , "arjun" , "abhi"]

for i in guest:
    print(f"invitation to {i} has been sent")

changes = input("who can’t make it")
new_guest = input("new guest")

guest.remove(changes)
guest.append(new_guest)

for i in guest:
    print(f"invitation to {i} has been sent")

"""3-6. More Guests: You just found a bigger dinner table, so now more space is 
available. Think of three more guests to invite to dinner.
•	 Start with your program from Exercise 3-4 or Exercise 3-5. Add a print()
call to the end of your program informing people that you found a bigger 
dinner table.
•	 Use insert() to add one new guest to the beginning of your list.
•	 Use insert() to add one new guest to the middle of your list.
•	 Use append() to add one new guest to the end of your list.
•	 Print a new set of invitation messages, one for each person in your list.
"""
guest = ["parth" , "arjun" , "abhi"]

for i in guest:
    print(f"invitation to {i} has been sent")


new_guest1 = input("new guest1")
guest.insert(0 , new_guest1)

new_guest2= input("new guest2")
guest.insert(int(len(guest)/2) , new_guest2)

new_guest3 = input("new guest3")
guest.append(new_guest3)

for i in guest:
    print(f"invitation to {i} has been sent")

"""3-7. Shrinking Guest List: You just found out that your new dinner table won’t 
arrive in time for the dinner, and you have space for only two guests.
•	 Start with your program from Exercise 3-6. Add a new line that prints a 
message saying that you can invite only two people for dinner.
•	 Use pop() to remove guests from your list one at a time until only two 
names remain in your list. Each time you pop a name from your list, print 
a message to that person letting them know you’re sorry you can’t invite 
them to dinner.
•	 Print a message to each of the two people still on your list, letting them 
know they’re still invited.
•	 Use del to remove the last two names from your list, so you have an empty 
list. Print your list to make sure you actually have an empty list at the end 
of your program"""

guest = ["parth" , "arjun" , "abhi"]

for i in guest:
    print(f"invitation to {i} has been sent")


new_guest1 = input("new guest1")
guest.insert(0 , new_guest1)

new_guest2= input("new guest2")
guest.insert(int(len(guest)/2) , new_guest2)

new_guest3 = input("new guest3")
guest.append(new_guest3)

for i in guest:
    print(f"invitation to {i} has been sent")

print("i can only invite 2 friends")
print(guest)
length = len(guest)

condition= True
i= 0
while condition:
        # print(f"{guest[i]} , i am sorry i cant  invite you to dinner.")
        pop = guest.pop()
        print(f"{pop} , i am sorry i cant  invite you to dinner.")
        if len(guest)== 2:
            condition =False
        i+=1

for i in guest:
    print(f"{i}  , your still invited")

print(guest)
print(bool(guest))

i=len(guest)-1
while guest:
    del guest[i]
    i-=1
print(guest)

"""sorting"""

list1 = ["p" , "m" , "b" , "g" , "z"]
print(list1)

list1.sort()
print(list1)

list1.reverse()
print(list1)

list1 = ["p" , "m" , "b" , "g" , "z"]
list1.sort(reverse=True)
print(list1)

list1 = ["p" , "m" , "b" , "g" , "z"]

print(sorted(list1))

print(list1)


