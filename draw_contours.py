import numpy as np
import cv2
im = cv2.imread('G:\Parth`s Space\Internship\data\opencv.png')
imgray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)


"""threshold  - gruping pxl values by =<127>="""

#255/2= 127
ret , threshold = cv2.threshold(imgray , 127 , 255 , 0)


cv2.imshow("th" , threshold)
cv2.waitKey(0)
cv2.destroyAllWindows()

"""
Contours is py list of all countours inimages , 
each individual Contour is numpy array of(x , y) boundary point of object"""

contours , hirrarchy = cv2.findContours(threshold , cv2.RETR_TREE , cv2.CHAIN_APPROX_NONE)

print("no. of Contours :"  , len(contours))
print(contours[0])

cv2.drawContours(im , contours , -1 , (0  , 0 , 255) , 3)


cv2.imshow("im" , im)
cv2.imshow("image_gray" , imgray)
cv2.waitKey(0)
cv2.destroyAllWindows()

