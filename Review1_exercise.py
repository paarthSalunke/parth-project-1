import os
import cv2
from tqdm import tqdm
import numpy as np

files = os.listdir(fr"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts")


#extract img and text file path
text_path = []
image_path = []

for i in files:
    if i[-4:] == ".txt":

        path = fr"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts"
        text_path.append(os.path.join(path , i))
    else:
        path = fr"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts"
        image_path.append(os.path.join(path , i))

text_path.sort()
image_path.sort()

####################################################################

#extracting annotation

print(image_path[0])
image = cv2.imread(image_path[0])
height , width , channel = image.shape

def yolo_to_coco(annot):
  X_center = float(annot[1])*width
  y_center = float(annot[2]) *height
  bbox_height = float(annot[3])*width
  bbox_width = float(annot[4])*height

  X_min = X_center - (bbox_height/2)
  y_min = y_center - (bbox_width/2)

  X_max = X_min +bbox_height
  y_max = y_min +bbox_width

  return int(X_min) , int(y_min) , int(X_max) , int(y_max)

####################################################################

yolo_annotations = []
for path in text_path:
  annot_img = []
  with open(path) as text:
    for lines in text.readlines():
      annot_img.append(lines.strip().split(" "))
  yolo_annotations.append(annot_img)

#####################################################################

#coco annotations

annotations_coco = []
class_count = []
for i in yolo_annotations:
  annot = []
  for j in i:
    annot.append(list(yolo_to_coco(j)))
  annotations_coco.append(annot)

#####################################################################

#storing bounding box images in list images

list_img = []
for i in tqdm(range(0 , len(image_path))):
  img = cv2.imread(image_path[i])
  for j in annotations_coco[i]:
    X_min ,y_min , X_max , y_max  = j
    img = cv2.rectangle(img, (X_min , y_min ), (X_max , y_max), (0 , 0 , 255), 2)
  list_img.append(img)


#########################################################################
#video write
video_name = 'video_review_1.avi'
video = cv2.VideoWriter(video_name, 0, 1, (width,height))

#writing each images
for img in list_img:
    video.write(img)

cv2.destroyAllWindows()
video.release()

#read output video

vid_capture = cv2.VideoCapture(fr'G:\Parth`s Space\Internship\video_review_1.avi')

ret, frame = vid_capture.read()
print(ret)
print(np.shape(frame))


while(vid_capture.isOpened()):
    #reading each frame
    ret, frame = vid_capture.read()

    if ret == True:
        #viz each frame in seq
        cv2.imshow('Frame', frame)
        key = cv2.waitKey(20)

        if key == ord('q'):
            break
    else: break

# Release the video capture object
vid_capture.release()
cv2.destroyAllWindows()

