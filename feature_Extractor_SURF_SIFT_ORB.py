import cv2
import numpy as np

#if we add 0 to imread function then we will get gray scale image
img1 = cv2.imread("G:\Parth`s Space\Internship\data\surf sift ord images\img1.jfif" )
img2 = cv2.imread("G:\Parth`s Space\Internship\data\surf sift ord images\image2.jfif" )

#orb object default is 500 features we are using n_features = 1000
orb = cv2.ORB_create(nfeatures=1000)

#extract key points and descripter from image 1 and 2
kp1 , des1 = orb.detectAndCompute(img1 , None)
kp2 , des2 = orb.detectAndCompute(img2 , None)

#total no. of features extracted
print("total no. of features extracted:" , des1.shape)
des1


#drawing keypoint on images
imgkpl1 = cv2.drawKeypoints(img1 , kp1 , None)
imgkpl2 = cv2.drawKeypoints(img2 , kp2 , None)


#show img
cv2.imshow("img1" , img1)
cv2.imshow("img2" , img2)

#show img with keypoints
cv2.imshow("imgkpl1" , imgkpl1)
cv2.imshow("imgkpl2" , imgkpl2)

cv2.waitKey(0)
cv2.destroyAllWindows()


#maching two images
"""their are various descripter , one of the easy one is Brut force. matching each 
descripter with other image desc . we can also use KNN algo"""

#bruteforce KNN algo and k =2
bf = cv2.BFMatcher()
matches = bf.knnMatch(des1, des2 , k=2)
print(matches[0])
good  = []

#matching how close
for m , n in matches:
    if m.distance < 0.85*n.distance:
        good.append([m])


print("no of good features for img 1 and 2:" , len(good))

#draw matches
img3 = cv2.drawMatchesKnn(img1 , kp1 ,img2 , kp2 , good ,None , flags=2 )

cv2.imshow("imgkpl1" , img1)
cv2.imshow("imgkpl2" , img2)
cv2.imshow("imgkpl3" , img3)
cv2.waitKey(0)
cv2.destroyAllWindows()