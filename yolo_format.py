import cv2
import numpy as np

#file path
file_path = r"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts\00001.txt"
image_path = r"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts\00001.jpg"

#extract annotations from text file
with open(file_path) as f:
    annot_lst = []
    for line in f:
        annot_lst.append(line.strip().split())

print(annot_lst)

#read image
img = cv2.imread(image_path)
# cv2.imshow("img" , img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

#extract width_img and height_img
print(img.shape)
height_img = img.shape[0]
width_img  = img.shape[1]
print("height_img :", height_img, "width_img :", width_img)


#extract X_min , y_min , X_max , y_max
"""yolo foramt provide us x_center , y_center and height_img , width_img of bounding box
then we convert it into X_min , y_min , X_max , y_max"""
annot = annot_lst[0]
print(annot)
x_center = float(annot[1]) * width_img
y_center = float(annot[2]) * height_img
bbox_width = float(annot[3]) * width_img
bbox_height = float(annot[4]) * height_img

#X_min , y_min , X_max , y_max
x_min = int(x_center - (bbox_width / 2))
y_min = int(y_center - (bbox_height / 2))

x_max =int(x_min + bbox_width)
y_max = int(y_min + bbox_height)

print("height_img :", height_img, "width_img :", width_img)
print((x_min, y_min), (x_max , y_max))

#draw annoted rectangle
img = cv2.rectangle(img, (x_min , y_min ), (x_max , y_max), (0 , 0 , 255), 2)


#show annoted image
cv2.imshow("parth", img)
cv2.waitKey(0)
cv2.destroyAllWindows()










