import cv2
import numpy as np

#generating white colour image
img_1 = np.zeros([512,512,3],dtype=np.uint8)

#code for white color
img_1.fill(255)
cv2.imshow('img', img_1)
cv2.waitKey(0)
cv2.destroyAllWindows()

#############################################################

#draw circle on image

center = (200 , 200)
radius = 25
color = (255 , 0 , 0)
thickness = 2
img_circle= cv2.circle(img_1, center, radius, color, thickness)
cv2.imshow('img_circle', img_circle)
cv2.waitKey(0)
cv2.destroyAllWindows()

######################################################################

#draw rectangle on image

x1 , y1 , x2 , y2 = 200 , 200 , 400 , 400
img_rec = cv2.rectangle(img_1, (x1, y1), (x2, y2), (255,0,0), 2)
cv2.imshow('img_rec', img_rec)
cv2.waitKey(0)
cv2.destroyAllWindows()


###########################################################################

#draw line on image
start_point = (350 , 350)
end_point =(500 , 500)
color = (0 , 255 , 0)
thickness = 3
image = cv2.line(img_1, start_point, end_point, color, thickness)
cv2.imshow('img_line', image)
cv2.waitKey(0)
cv2.destroyAllWindows()

###########################################################################

#write text on image

img = cv2.putText(img_1,"Hello World!!!", (40, 500), cv2.FONT_HERSHEY_SIMPLEX, 2, 255)
cv2.imshow('text on image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
