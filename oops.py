# simple classs creation
class computer:
    def config(self):
        print(" i5 , 16gb")


com1 = computer()
com1.config()


# __init__ we use init to initialize variables
class computer1:
    def __init__(self, cpu, ram):
        self.cpu = cpu
        self.ram = ram
        self.name = "dell"

    def config(self):
        print(self.cpu, self.ram, self.name)

    def update(self):
        self.cpu_update = "i10"
        print(self.cpu_update)


com1 = computer1("i5", "16gb")
com2 = computer1("i7", "8gb")
com1.config()
com1.update()

com2.config()

"""variables  -  1. instance var 2. class var 
instance var - instance var  are different for diff objects example cpu and
ram in class computer2 diferent 

class var - if we change value of class var then it will affect all other
object also.
"""


class computer2:
    laptop_name = "lenova"  # class var

    def __init__(self, cpu, ram):
        self.cpu = cpu  # instance var
        self.ram = ram  # instance var


com1 = computer2("i5", "16gb")
com2 = computer2("i7", "16gb")

print(com1.laptop_name)

com1.laptop_name = "dell"
print(com1.laptop_name)
print(com2.laptop_name)

"""methods / function = behaviour
instance method , class method , static method"""


class student:
    school_name = "mps"  # static var

    def __init__(self, m1, m2, m3):
        self.m1 = m1  # instance var
        self.m2 = m2
        self.m3 = m3

    def avg(self):  # instance method #if you are working with instance var then use "self"
        self.sum = self.m1 + self.m2 + self.m3
        avg = self.sum / 3
        return (avg)

    @classmethod
    def school(cls):  # class method #if you are working with class var then use "cls"
        return cls.school_name  # access class var  = class.var

    @staticmethod
    def info():  # static method #when your function dont use instance and static var
        print("it is class 7th students")


s1 = student(6, 7, 8)
s2 = student(9, 6, 7)

print(s1.avg())
print(s1.school())
print(student.school())


# class inside class

class student:
    def __init__(self, name, roll_no):
        self.name = name
        self.roll_no = roll_no
        self.lap_class = self.laptop()  # creating object of inner class in outer class

    def show(self):
        return self.name, self.roll_no

    class laptop:
        def __init__(self):
            self.brand = "hp"
            self.cpu = "15"
            self.ram = "16gb"


s1 = student("parth", 31)
s2 = student("arjun", 32)

print(s1.name, s2.name)
print(s1.show())

print(s1.lap_class.brand)

lap1 = student.laptop()  # creating object of inner class laptop
print(lap1.brand)

# inheritance

class a:
    def feature1(self):
        return "feature 1"
    def feature2(self):
        return "feature 2"

class b(a):       #class b inheritance all feature of class a
    def feature3(self):
        return "feature 2"
    def feature4(self):
        return "feature 4"

parth = b()
print(parth.feature4() , parth.feature3())
print(parth.feature1() , parth.feature2())     #now we can also access features of class a in class b object


#multilevel inheritance

class a:
    def feature1(self):
        return "feature 1"

    def feature2(self):
        return "feature 2"


class b(a):  # class b inheritance all feature of class a
    def feature3(self):
        return "feature 2"

    def feature4(self):
        return "feature 4"

class c(b):  # class c inheritance all feature of class a and b
    def feature5(self):
        return "feature 5"

    def feature6(self):
        return "feature 6"


parth = c()  #object of class c
print(parth.feature4(), parth.feature3())
print(parth.feature1(), parth.feature2())  # now we can also access features of class a in class b object
print(parth.feature5(), parth.feature6())

#multiple inheritance when a and c dont have any relation
class a:
    def feature1(self):
        return "feature 1"

    def feature2(self):
        return "feature 2"


class b():  # class b inheritance all feature of class a
    def feature3(self):
        return "feature 2"

    def feature4(self):
        return "feature 4"


class c(a , b):  # class c inheritance all feature of class a and b   ,  a and b dont have any relation
    def feature5(self):
        return "feature 5"

    def feature6(self):
        return "feature 6"


parth = c()  # object of class c
print(parth.feature4(), parth.feature3())
print(parth.feature1(), parth.feature2())  # now we can also access features of class a in class b object
print(parth.feature5(), parth.feature6())


#constructor in inherantance
"""when you want to access __init__ var of class a in class b"""

class a:
    def __init__(self):
        print("class a init")
    def feature1(self):
        return "feature 1a"



class b(a):

    def __init__(self):
        super().__init__()   #when you want to use init of class a also
        print("class b init")

    def feature4(self):
        return "feature 2b"

parth = b()
print(parth.feature1())

#constructor in inherantance
"""when you want to access __init__ var of class a and b   in class c"""


class a:
    def __init__(self):
        print("class a init")

    def feature1(self):
        return "feature 1a"


class b(a):

    def __init__(self):
        super().__init__()  # when you want to use init of class
        print("class b init")

    def feature4(self):
        return "feature 2b"


parth = b()
print(parth.feature1())



#using super to call method of class a in class b


class a:
    def __init__(self):
        print("class a init")

    def feature1(self):
        return "feature 1a"


class b(a):

    def __init__(self):
        super().__init__()
        print("class b init")

    def feature4(self):
        return "feature 2b"

    def feat_a(self):
        return super().feature1()  #calling feature of class a in class b

parth = b()
print(parth.feat_a())


#polymorphism
""" 1 duck typing
    2. operator overloading 
    3. method overloading
    4.method overriding"""

#duck Typing











