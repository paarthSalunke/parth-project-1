# coco Annotation format
"""
[x1 , y1 , x2 , y2]
x1,y1 ------
|          |
|          |
|          |
--------x2,y2

"""
import cv2
import json
import urllib3
from PIL import Image
import numpy as np
import requests

# loading json file
instances = open(r'G:\Parth`s Space\Internship\data\annotations\instances_val2017.json')
data_instances = json.load(instances)

# img url
img_id = str(data_instances["annotations"][0]["image_id"])
url = "http://images.cocodataset.org/val2017/000000" + img_id + ".jpg"
img = Image.open(requests.get(url, stream=True).raw)

#converting to numpy array
img = np.array(img)

# converting BGR to RGB
# img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

#plot image
cv2.imshow("parth", img)
cv2.waitKey(0)  # wait for a keyboard input
cv2.destroyAllWindows()

print(data_instances["annotations"][0]["bbox"])
bbox = data_instances["annotations"][0]["bbox"]

# [473.07, 395.93, 38.65, 28.67]  [x1,y1,x2,y2]
# cv2.rectangle(img, (x1, y1), (x2, y2), (255,0,0), 2)

# converting float to int
x1, y1, x2, y2 = int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3])

# draw annotation box
img = cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 2)

#plot image
cv2.imshow("parth", img)
cv2.waitKey(0)
cv2.destroyAllWindows()
