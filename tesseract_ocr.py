# -*- coding: utf-8 -*-
"""tesseract OCR.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1-g4ZqmaX8GqIjU35eESUSH1gnvPKofR9

# **OCR**
"""

!pip install torch torchvision torchaudio
!pip install easyocr

import cv2
import easyocr
import matplotlib.pyplot as plt
from google.colab.patches import cv2_imshow

#reading image
image_path = "/content/hello world.png"

#extract text from image
reader = easyocr.Reader(["en"] ,  gpu=False)
result = reader.readtext(image_path)
result

#bounding box for two words
x1_y1_0  = tuple(result[0][0][0])
x2_y2_0 = tuple(result[0][0][2]) 

x1_y1_1  = tuple(result[1][0][0])
x2_y2_1 = tuple(result[1][0][2])

#draw bounding box arround text
img = cv2.imread(image_path) 
img = cv2.rectangle(img , x1_y1_0 , x2_y2_0 , (255 , 0 , 0) , 2)
img = cv2.rectangle(img , x1_y1_1 , x2_y2_1 , (0   , 255 , 0) , 2)
cv2_imshow(img)

"""# **pytesseract**"""

#install py tesseract
!sudo apt install tesseract-ocr
!pip install pytesseract

import pytesseract
import shutil
import os
import random
from PIL import Image

#read image
image_path_in_colab="/content/img_ocr.png"
img = Image.open(image_path_in_colab)

#extract txt from image
extractedInformation = pytesseract.image_to_string(img)
print(extractedInformation)



