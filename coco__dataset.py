import json
from PIL import Image
import requests

# caption data reading
captions = open(r'G:\Parth`s Space\Internship\data\annotations\captions_val2017.json')
captions_data = json.load(captions)

# total no. of val images
print("total no. of validation images ", len(captions_data["images"]))

# keys in  json files
print(captions_data.keys())

# print image caption
print(captions_data["annotations"][0]["caption"])

# open image from url
url = "http://images.cocodataset.org/val2017/000000" + str(179765) + ".jpg"
im = Image.open(requests.get(url, stream=True).raw)
im.show()

##############################################################################################################

# reading instance / annotation data
instances = open(r'G:\Parth`s Space\Internship\data\annotations\instances_val2017.json')
data_instances = json.load(instances)

# print keys in jason file
print(data_instances.keys())

# bounding box  & image id
print(data_instances["annotations"][0]["bbox"], data_instances["annotations"][0]["image_id"])

# opening image
img_id = str(data_instances["annotations"][0]["image_id"])
url = "http://images.cocodataset.org/val2017/000000" + img_id + ".jpg"
im = Image.open(requests.get(url, stream=True).raw)
im.show()

##############################################################################################################

# rading instance / annotation data
keyspoints = open(r'G:\Parth`s Space\Internship\data\annotations\person_keypoints_val2017.json')
data_keyspoints = json.load(keyspoints)

print(data_keyspoints.keys())

# bounding box annotations and category id
print("bbox :", data_keyspoints["annotations"][0]["bbox"] \
      , "category_id :", data_keyspoints["annotations"][0]["category_id"])

print(data_keyspoints["categories"])
