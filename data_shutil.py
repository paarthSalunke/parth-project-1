import cv2
import tensorflow as tf
import os
import shutil

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

os.chdir(r"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts")

files = os.listdir(fr"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts")

print(files)


#copy file from one dir to another dir
for i in files:
    if i[-4:] == ".txt":

        path = fr"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts"
        text_path = os.path.join(path , i)
        new_path = fr"G:\Parth`s Space\Internship\data\traffic tf format\labels"
        new_text_path = os.path.join(new_path , i)
        shutil.copy(text_path, new_text_path)
    else:
        path = fr"G:\Parth`s Space\Internship\data\yolo traffic dataset\ts"
        image_path=os.path.join(path , i)
        new_path = fr"G:\Parth`s Space\Internship\data\traffic tf format\images"
        new_img_path = os.path.join(new_path , i)
        shutil.copy(image_path, new_img_path)


