import cv2

# Load the cascade
#https://github.com/opencv/opencv/tree/master/data/haarcascades
face_cascade = cv2.CascadeClassifier('G:\Parth`s Space\Internship\data\haarcascade_frontalface_default.xml')


# Read the input image
img = cv2.imread(fr'G:\Parth`s Space\Internship\data\faces.jpg')

#convering to grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Detecting faces
faces = face_cascade.detectMultiScale(gray, 1.1, 4)

# draw bounding box around the faces

for (x, y, w, h) in faces:
    cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)


cv2.imshow('img', img)
cv2.waitKey(0)
cv2.destroyAllWindows()


#face detection live web cam

vid_capture = cv2.VideoCapture(0, cv2.CAP_DSHOW)

#while loop will go through each frame
while(vid_capture.isOpened()):
    #reading each frame
    ret, frame = vid_capture.read()
    gray = cv2.cvtColor(frame , cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

    if ret == True:
        #viz each frame in seq
        cv2.imshow('Frame', frame)
        key = cv2.waitKey(20)

        if key == ord('q'):
            break
    else: break

# Release the video capture object
vid_capture.release()
cv2.destroyAllWindows()