import cv2

#read video
vid_path = r"G:\Parth`s Space\Internship\data\yolo traffic dataset\traffic-sign-to-test.mp4"
vid_capture = cv2.VideoCapture(vid_path)

#frame
fps = vid_capture.get(7)
print(fps)

# cv2.VideoCapture – Creates a video capture object, which would help stream or display the video.
#vid_capture.isOpened() it will be true  , if any error occour while reading it will output False
print(vid_capture.isOpened())


#while loop will go through each frame
while(vid_capture.isOpened()):
    #reading each frame
    ret, frame = vid_capture.read()

    if ret == True:
        #viz each frame in seq
        cv2.imshow('Frame', frame)

        # waitKey() function to pause for 20ms between video frames.
        # Calling the waitKey() function lets you monitor the keyboard for user input.
        # In this case, for example, if the user presses the ‘q’ key, you exit the loop.
        key = cv2.waitKey(20)

        if key == ord('q'):
            break
    else: break

# Release the video capture object
vid_capture.release()
cv2.destroyAllWindows()



"""reading video from webcam"""


#video cature from web cam
#CAP_DSHOW is  video-capture API preference, which is short for directshow via video input.
vid_capture = cv2.VideoCapture(0, cv2.CAP_DSHOW)
print(vid_capture)

#while loop will go through each frame
while(vid_capture.isOpened()):
    #reading each frame
    ret, frame = vid_capture.read()

    if ret == True:
        #viz each frame in seq
        cv2.imshow('Frame', frame)

        # waitKey() function to pause for 20ms between video frames.
        # Calling the waitKey() function lets you monitor the keyboard for user input.
        # In this case, for example, if the user presses the ‘q’ key, you exit the loop.
        key = cv2.waitKey(20)

        if key == ord('q'):
            break
    else: break

# Release the video capture object
vid_capture.release()
cv2.destroyAllWindows()



