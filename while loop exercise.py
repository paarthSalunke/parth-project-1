# 1 counting
count_value = 1

while count_value <= 5:
    print(count_value)
    count_value += 1

##############################################################################################

"""2 Pizza Toppings: Write a loop that prompts the user to enter a series of
pizza toppings until they enter a 'quit' value. As they enter each topping,
print a message saying you’ll add that topping to their pizza. Pepperoni,Mushroom,Extra cheese.

Green pepper."""

accept = True
while accept:
    user_input = input()

    if user_input == "quit":
        accept = False
    else:
        print(f"{user_input} =  added to pizza")

##############################################################################################

"""2 Movie Tickets: A movie theater charges different ticket prices depending on
a person’s age. If a person is under the age of 3, the ticket is free; if they are
between 3 and 12, the ticket is $10; and if they are over age 12, the ticket is
$15. Write a loop in which you ask users their age, and then tell them the cost
of their movie ticket."""

price = 0
accept = True
while accept:
    user_age = input()
    if user_age == "quit":
        accept = False
    elif int(user_age) == 3:
        continue
    elif 3 < int(user_age) < 12:
        price += 10
    elif int(user_age) > 12:
        price += 15

    print(f"price of ticket is {price}")

##############################################################################################

# 4 removing specific animal from list
animals = ["cat", "dog", "cat", "dog", "cat", "cat", "cat"]

while "cat" in animals:
    animals.remove("cat")

print(animals)

##############################################################################################

"""7-8. Deli: Make a list called sandwich_orders and fill it with the names of vari-
ous sandwiches. Then make an empty list called finished_sandwiches. Loop

through the list of sandwich orders and print a message for each order, such
as I made your tuna sandwich. As each sandwich is made, move it to the list
of finished sandwiches. After all the sandwiches have been made, print a
message listing each sandwich that was made."""

unfinish_sandwitch = ["sw1" , "sw2" , "sw3" , "sw4"]
finish_sandwitch = []

while unfinish_sandwitch:
    item = unfinish_sandwitch.pop()

    finish_sandwitch.append(item)

for i in finish_sandwitch:
    print("finish sandwitch = " , i)

##############################################################################################

"""7-9. No Pastrami: Using the list sandwich_orders from Exercise 7-8, make sure
the sandwich 'pastrami' appears in the list at least three times. Add code
near the beginning of your program to print a message saying the deli has
run out of pastrami, and then use a while loop to remove all occurrences of
'pastrami' from sandwich_orders. Make sure no pastrami sandwiches end up
in finished_sandwiches."""

unfinish_sandwitch = ["sw1" , "pastrami" , "sw3" , "pastrami" , "pastrami"]
finish_sandwitch = []

while unfinish_sandwitch:
    item = unfinish_sandwitch.pop()
    if item == "pastrami":
        print("deli has run out of pastrami")
    else:
        finish_sandwitch.append(item)

for i in finish_sandwitch:
    print("finish sandwitch = " , i)