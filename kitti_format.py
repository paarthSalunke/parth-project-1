import cv2
import os

files = os.listdir(fr"G:\Parth`s Space\Internship\data\kitti dataset")

img_path = []
text_path = []

for i in files:
    if i[-4:] == ".png" :
        path = fr"G:\Parth`s Space\Internship\data\kitti dataset\{i}"
        img_path.append(path)
    else:
        path = fr"G:\Parth`s Space\Internship\data\kitti dataset\{i}"
        text_path.append(path)

#read text file kitti format
with open(text_path[0]) as f:
    a = f.readline()
    annot = a.split(" ")

#kitti file
# Pedestrian 0.00 0 -0.20 712.40 143.00 810.73 307.92 1.89 0.48 1.20 1.84 1.47 8.41 0.01
""" class name , truncation  , Occlusion , alpha , Bounding box coordinates: [xmin, ymin, xmax, ymax] 
3-D dimension , location , rotation_y"""

#read img
img = cv2.imread(img_path[0])

#extract x_min , y_min , x_max , y_max from text file kitti
x_min , y_min , x_max , y_max = int(float(annot[4])) , int(float(annot[5]))  , int(float(annot[6]))  , int(float(annot[7]))
print(x_min , y_min , x_max , y_max )

#draw rectangle
img = cv2.rectangle(img, (x_min , y_min ), (x_max , y_max), (0 , 0 , 255), 2)

#viz img
cv2.imshow("im1" , img)
cv2.waitKey(0)
cv2.destroyAllWindows()

