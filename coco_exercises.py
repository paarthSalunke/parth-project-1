import cv2
import json
# import urllib3
from PIL import Image
import numpy as np
import requests
import matplotlib.pyplot as plt

# caption data reading
captions = open(r'G:\Parth`s Space\Internship\data\annotations\captions_val2017.json')
captions_data = json.load(captions)

# reading instance / annotation data
instances = open(r'G:\Parth`s Space\Internship\data\annotations\instances_val2017.json')
data_instances = json.load(instances)

# reading person keypoints /
person_keypoints= open(r'G:\Parth`s Space\Internship\data\annotations\person_keypoints_val2017.json')
data_person_keypoints = json.load(person_keypoints)


print(captions_data.keys())
print(data_instances.keys())
print(data_person_keypoints.keys())

#how many classes ?
print(data_instances["categories"])
no_of_categories = len(data_instances["categories"])
print(f"dataset contain {no_of_categories} categories")

#how many images per classes


print(data_instances.keys())
print(data_instances['annotations'][0]['category_id'])

#storing category id
categories_list =[]
for i in data_instances['annotations']:
    categories_list.append(i['category_id'])
print(categories_list)
#storing images per classes in dict
img_per_classes ={}

unique_classes = list(set(categories_list))
for i in unique_classes:
    img_per_classes[i] = categories_list.count(i)

print("images per class by category id" , img_per_classes)


#data is imbalance?

print(img_per_classes.values())


plt.bar(img_per_classes.keys(), img_per_classes.values(),  color='g')

"""yes, data is imbalance for some classes"""














